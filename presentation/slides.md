---
title:          Qwixx
author:
  - Severin Kaderli
  - Marius Schär
date:           2018-06-15
lang:           de-CH
links-as notes: true
classoption:
  - aspectratio=169
mainfont:       Helvetica Neue
mathfont:       Latin Modern Math
monofont:       Source Code Pro

---


## Source
\center{\LARGE{qwixx.mariusschaer.ch}} ^[Link](https://qwixx.mariusschaer.ch)^

# Design

## Player
![Class Diagram](pictures/class_diagram_player.png){height=90%}

## Sheet
![lass Diagram](pictures/class_diagram_sheet.png){height=90%}

## Controls
![class Diagram](pictures/class_diagram_controls.png){height=90%}

# Besonders
## Enforcer
\colsbegin
\column{.6\textwidth}

- Alle Kreuzversuche müssen hier durch
- Überprüft ob dieser Versuch valide ist
- Überprüft ob dieser Spieler das Feld kreuzen darf

\column{.4\textwidth}

![Enforcer](pictures/enforcer.png){width=60%}

\colsend

## Difficulties
- `ObjectProperty<>::addListener()`
- `alignment=...` nicht konsistent zwischen Elementen
- Mensch/CPU-Mix: unterbrechen für andere Spieler

## Lessons Learned
- Früher fertig \& mehr testing
- UML kann hilfreich sein zum Entwickeln
- Design für kompletes Spiel grob festlegen

## Demo
\center{\LARGE{Demo}}

## Q\&A
\center{\LARGE{Q\&A}}

## Danke

\begin{center}
\LARGE{Danke}

\Large{qwixx.mariusschaer.ch}
\end{center}
