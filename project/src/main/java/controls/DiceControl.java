package controls;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import model.Dice;
import utils.CSS;
import utils.CustomControlHelper;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A Window which displays all {@link Dice}
 */
public class DiceControl extends AnchorPane implements CustomControl {

    private static final String FXML = "dice_control.fxml";
    private static final String ID_PREFIX = "id.die_card.";

    private final List<DieControl> dieControls;

    @FXML
    private Button rollButton;

    @FXML
    private GridPane diceGridPane;

    @FXML
    private SplitPane diceSplitPlane;

    public DiceControl() {
        CustomControlHelper.load(this);

        this.setId(ID_PREFIX + "self");
        this.dieControls = Dice.getDice().keySet().stream() //
                .sorted(Comparator.comparingInt(a -> a.getColour().ordinal())) //
                .map(DieControl::new).collect(Collectors.toList());

        initializeGridPane();
        initializeRollButton();
    }

    private void initializeRollButton() {
        String background = CSS.Background.generate(CSS.LIGHT);
        String foreground = CSS.Foreground.generate(CSS.DARK);
        String border = CSS.Border.generate(CSS.DARK);
        String font = CSS.Font.Size.generate(23);
        CSS.Applicator.set(this.rollButton, background, foreground, border, font);

        this.rollButton.setText("⚄");
        this.rollButton.setOnAction(e -> Dice.roll());
    }

    private void initializeGridPane() {
        for (int col = 0; col < dieControls.size(); col++) {
            DieControl die = dieControls.get(col);
            GridPane.setColumnIndex(die, col);
            GridPane.setRowIndex(die, 0);
        }
        this.diceGridPane.getChildren().addAll(dieControls);
    }

    @Override
    public String getFXMLFile() {
        return FXML;
    }
}
