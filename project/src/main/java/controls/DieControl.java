package controls;

import javafx.beans.binding.Bindings;
import javafx.scene.control.Label;
import javafx.util.converter.NumberStringConverter;
import model.Die;
import utils.CSS;
import utils.CustomControlHelper;

public class DieControl extends Label implements CustomControl {

    private static final String FXML = "die.fxml";

    private final Die die;

    private static final String ID_PREFIX = "id.die.";

    public DieControl(Die die) {
        CustomControlHelper.load(this);

        this.die = die;
        this.setId(ID_PREFIX + die.getColour().name());

        this.setId(this.getId() + "dieLabel");

        // Bind the value of the Die to always be displayed
        // Use bindBidirectional because Unidirectional doesn't support Converters
        Bindings.bindBidirectional(this.textProperty(), this.die.getValueProperty(), new NumberStringConverter(){
            /**
             *  Replaces 0 with - to indicate more clearly that this Die is disabled.
             */
            @Override
            public String toString(Number value) {
                if(value.intValue() == 0){
                    return "-";
                }
                return super.toString(value);
            }
        });

        addStyle(this);
    }

    /**
     * Adds Background and Foreground color, based on the Die Colour.
     */
    private void addStyle(Label label) {
        String background = CSS.Background.generate(this.die.getColour());
        String foreground = CSS.Foreground.generate(CSS.Contrast.generate(this.die.getColour()));
        String border = CSS.Border.generate(CSS.DARK);
        String font = CSS.Font.Size.generate(40);
        CSS.Applicator.set(label, background, foreground, border, font);
    }

    @Override
    public String getFXMLFile() {
        return FXML;
    }
}
