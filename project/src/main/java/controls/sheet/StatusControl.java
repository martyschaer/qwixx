package controls.sheet;

import controls.CustomControl;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Button;
import model.player.Status;
import model.sheet.Sheet;
import utils.CustomControlHelper;

/**
 * One of the coloured rows in each {@link model.player.Player}s {@link Sheet}
 */
public class StatusControl extends Button implements CustomControl {

    private static final String FXML = "play_status.fxml";

    private ObjectProperty<Status> status = new SimpleObjectProperty<>(Status.WAITING);

    public StatusControl(){
        CustomControlHelper.load(this);
        this.setOnAction(actionEvent -> this.status.setValue(Status.WAITING));
    }

    public void addStatus(ObjectProperty<Status> statusProperty){
        this.status.bindBidirectional(statusProperty);
        this.status.addListener((observableValue, oldUnused, newStatus) -> updatePlayStatus(newStatus));
        updatePlayStatus(this.status.get());
    }

    private void updatePlayStatus(Status status){
        this.textProperty().set(status.text());
        this.getStyleClass().remove(status.getOther().cssClass());
        this.getStyleClass().add(status.cssClass());

        //Disable the button while not playing
        //So the button can be used for the player to indicate done-ness
        if(status.equals(Status.PLAYING)){
            this.setDisable(false);
        }else{
            this.setDisable(true);
        }
    }

    @Override
    public String getFXMLFile() {
        return FXML;
    }
}
