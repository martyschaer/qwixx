package controls.sheet;

import controls.CustomControl;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import model.Colour;
import model.rules.Enforcer;
import model.sheet.NumberField;
import utils.CSS;
import utils.CustomControlHelper;

import java.awt.*;

/**
 * The {@link model.sheet.Field}s which contain the numbers of each row.
 */
public class NumberFieldControl extends AnchorPane implements CustomControl {

    private static final String FXML = "number_field.fxml";

    @FXML
    private Button numberFieldButton;

    private static final String ID_PREFIX = "id.number_field.";

    private final NumberField field;

    public NumberFieldControl(NumberField field) {
        CustomControlHelper.load(this);
        this.field = field;
        initializeButton();

        this.setId(generateId());

        this.field.enabled().addListener((observableValue, oldUnused, newUnused) -> onStatusChanged());
        this.field.crossed().addListener((observableValue, oldUnused, newUnused) -> onStatusChanged());
    }

    private void initializeButton() {
        this.numberFieldButton.setId(generateId() + ".button");
        this.numberFieldButton.setText(String.valueOf(this.field.getValue()));
        setDefaultStyle();
        this.numberFieldButton.setOnAction(this::onClick);
    }

    /**
     * When clicked, attempts to cross out the field and disables any further action on a field.
     */
    private void onClick(Event e){
        // attempt to cross out the field
        if(Enforcer.cross(this.field.getRow().getSheet().getPlayer(), this.field.getRow(), this.field.getValue())){
            this.numberFieldButton.setDisable(true); //disable the button
        }
    }

    /**
     * Triggered when a field changes Enable or Cross status.
     * Used to set the styling of the field.
     */
    private void onStatusChanged(){
        if(this.field.isCrossed()){
            setCrossedStyle();
        }else if(!this.field.isEnabled()){
            setDisabledStyle();
        }
    }

    /**
     * Generates an ID from:
     *  - "id.number_field."
     *  - the colour name of the row.
     *  - the value of the field
     * Example:
     * id.number_field.RED.3
     */
    private String generateId(){
        return ID_PREFIX //
                + this.field.getRow().getColour().name() //
                + this.field.getValue();
    }

    /**
     * The default style, given by the color of the row, with nicely contrasted text and a dark border.
     */
    private void setDefaultStyle() {
        Colour colour = this.field.getRow().getColour();
        String background = CSS.Background.generate(colour);
        String foreground = CSS.Foreground.generate(CSS.Contrast.generate(colour));
        String border = CSS.Border.generate(CSS.DARK);
        CSS.Applicator.set(this.numberFieldButton, background, foreground, border);
    }

    /**
     * Like the default style, but darker, to show that this field can no longer be clicked.
     *
     * Applied to any fields to the right of a crossed field that aren't crossed, as well as all remaining fields
     * when the row is finished.
     */
    private void setDisabledStyle(){
        Color color = CSS.Disabled.generate(this.field.getRow().getColour());
        String background = CSS.Background.generate(color);
        String foreground = CSS.Foreground.generate(CSS.Contrast.generate(color));
        String border = CSS.Border.generate(CSS.DARK);
        CSS.Applicator.set(this.numberFieldButton, background, foreground, border);
    }

    private void setCrossedStyle(){
        Color color = CSS.Brighten.generate(this.field.getRow().getColour(), 1.6f);
        String background = CSS.Background.generate(color);
        String foreground = CSS.Foreground.generate(CSS.Contrast.generate(color));
        String border = CSS.Border.generate(CSS.DARK);
        String opacity = CSS.Opacity.full();
        String font = CSS.Font.Weight.bold();
        CSS.Applicator.set(this.numberFieldButton, background, foreground, border, opacity, font);
    }


    @Override
    public String getFXMLFile() {
        return FXML;
    }
}
