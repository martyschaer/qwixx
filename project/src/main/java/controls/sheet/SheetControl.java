package controls.sheet;

import controls.CustomControl;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.util.converter.NumberStringConverter;
import model.sheet.Sheet;
import utils.CustomControlHelper;

import java.util.List;
import java.util.stream.Collectors;

/**
 * A sheet. There exists one for each player.
 */
public class SheetControl extends AnchorPane implements CustomControl {

    private static final String FXML = "sheet.fxml";

    private List<RowControl> rows;

    @FXML
    private GridPane gridPaneSheet;

    @FXML
    private GridPane gridPaneRows;

    @FXML
    private TitledPane titlePlayerName;

    @FXML
    private StatusControl statusControl;

    @FXML
    private Label scoreLabel;

    @FXML
    private MissthrowControl missthrowControl;

    private final Sheet sheet;

    public SheetControl(Sheet sheet) {
        CustomControlHelper.load(this);
        this.sheet = sheet;
        initiateSubControls();
    }

    private void initiateSubControls() {
        initiateMissthrowControl();
        initiateStatusControl();
        initiateTitlePane();
        initiateRows();
        initiateScoreLabel();
    }

    private void initiateMissthrowControl(){
        this.missthrowControl.setSheet(this.sheet);
    }

    private void initiateStatusControl() {
        this.statusControl.addStatus(this.sheet.playStatus());
    }

    private void initiateTitlePane() {
        this.titlePlayerName.textProperty().set(this.sheet.getPlayerDisplayName());
    }

    private void initiateRows() {
        this.rows = this.sheet.getRows().stream().map(RowControl::new).collect(Collectors.toList());

        //set the Column index for every RowControl
        this.rows.forEach(row -> GridPane.setColumnIndex(row, 0));

        //set the Row index for every RowControl
        for(int i = 0; i < this.rows.size(); i++){
            GridPane.setRowIndex(this.rows.get(i), i);
        }
        this.gridPaneRows.getChildren().addAll(this.rows);
    }

    private void initiateScoreLabel() {
        Bindings.bindBidirectional(scoreLabel.textProperty(), this.sheet.scoreProperty(), new NumberStringConverter());
    }

    @Override
    public String getFXMLFile() {
        return FXML;
    }
}
