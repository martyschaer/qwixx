package controls.sheet;

import controls.CustomControl;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import model.Colour;
import model.sheet.FinishedField;
import model.sheet.Row;
import utils.CustomControlHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * One of the coloured rows in each {@link model.player.Player}s {@link model.sheet.Sheet}
 */
public class RowControl extends AnchorPane implements CustomControl {

    private static final String FXML = "row.fxml";

    @FXML
    private GridPane rowGridPane;
    private final Row row;
    private final List<NumberFieldControl> fields = new ArrayList<>();
    private final FinishedFieldControl finishedField;

    public RowControl(Row row) {
        CustomControlHelper.load(this);
        this.row = row;

        //number fields
        initiateFields();

        //finish field
        this.finishedField = new FinishedFieldControl(new FinishedField(this.row));

        //add all number fields to the control
        rowGridPane.addRow(0, // add every NumberFieldControl to the row
                this.fields.toArray(new Node[this.fields.size()]));

        //add finish field at the end of the row
        rowGridPane.add(finishedField, this.fields.size(), 0);
    }

    private void initiateFields() {
        this.fields.addAll(this.row.getFields() //
                .stream() //
                .map(NumberFieldControl::new) //
                .collect(Collectors.toList()));
    }

    public Colour getColour() {
        return this.row.getColour();
    }

    @Override
    public String getFXMLFile() {
        return FXML;
    }
}
