package controls.sheet;

import controls.CustomControl;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import model.Colour;
import model.sheet.FinishedField;
import utils.CSS;
import utils.CustomControlHelper;

import java.awt.*;

/**
 * The {@link model.sheet.Field} at the end of each row, which displays whether or not a row has been completed.
 */
public class FinishedFieldControl extends AnchorPane implements CustomControl {

    private static final String FXML = "finished_field.fxml";

    private static final String CHECK = "✓";
    private static final String CROSS = "×";

    @FXML
    private Label finishedFieldLabel;

    private static final String ID_PREFIX = "id.finished_field.";

    private final FinishedField field;

    public FinishedFieldControl(FinishedField field) {
        CustomControlHelper.load(this);
        this.field = field;
        initializeButton();

        this.setId(generateId());

        this.field.crossed().addListener((observableValue, oldBoolean, newBoolean) -> onFinishedChanged(newBoolean));
    }

    /**
     * Sets the styling of the field when a row is finished.
     */
    private void onFinishedChanged(boolean finished){
        if(finished){
            setFinishedStyle();
            this.finishedFieldLabel.setText(CHECK);
        }else{
            setDefaultStyle();
            this.finishedFieldLabel.setText(CROSS);
        }
    }

    private void initializeButton() {
        this.finishedFieldLabel.setId(generateId() + ".button");
        this.finishedFieldLabel.setText(CROSS);
        setDefaultStyle();
    }

    /**
     * Generates an ID from:
     *  - "id.finished_field."
     *  - the colour name of the row.
     * Example:
     * id.finished_field.RED
     */
    private String generateId(){
        return ID_PREFIX //
                + this.field.getRow().getColour().name();
    }

    /**
     * Sets the Field to the default style:
     *
     * still the Colour of the row, but much lighter.
     */
    private void setDefaultStyle() {
        Color colour = CSS.Brighten.generate(this.field.getRow().getColour(), 3);
        String background = CSS.Background.generate(colour);
        String foreground = CSS.Foreground.generate(CSS.Contrast.generate(colour));
        String border = CSS.Border.generate(CSS.DARK);
        String font = CSS.Font.Size.generate(22);
        CSS.Applicator.set(this.finishedFieldLabel, background, foreground, border, font);
    }

    /**
     * Sets the Field to the finished style:
     *
     * looks the same as the {@link NumberFieldControl}s next to it.
     */
    private void setFinishedStyle(){
        Colour colour = this.field.getRow().getColour();
        String background = CSS.Background.generate(colour);
        String foreground = CSS.Foreground.generate(CSS.Contrast.generate(colour));
        String border = CSS.Border.generate(CSS.DARK);
        String font = CSS.Font.Size.generate(20);
        CSS.Applicator.set(this.finishedFieldLabel, background, foreground, border, font);
    }

    @Override
    public String getFXMLFile() {
        return FXML;
    }
}
