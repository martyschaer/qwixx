package controls.sheet;

import controls.CustomControl;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import model.sheet.Sheet;
import utils.CustomControlHelper;

/**
 * One of the coloured rows in each {@link model.player.Player}s {@link model.sheet.Sheet}
 */
public class MissthrowControl extends GridPane implements CustomControl {

    private static final String FXML = "missthrows.fxml";
    private static final String CSS_CLASS_FAIL = "missthrow_indicator_fail";
    private static final String CSS_CLASS_OK = "missthrow_indicator_ok";
    private static final String MISSTHROW_STRING = "FAIL";

    private Sheet sheet;

    public MissthrowControl(){
        CustomControlHelper.load(this);
    }

    public void setSheet(Sheet sheet){
        this.sheet = sheet;
        this.sheet.missthrowProperty().addListener((observableValue, oldNumber, newNumber) -> {
            for(int i = 0; i < newNumber.intValue() && i < this.getChildren().size(); i++){
                Label indicator = (Label) this.getChildren().get(i);
                indicator.getStyleClass().remove(CSS_CLASS_OK);
                indicator.getStyleClass().add(CSS_CLASS_FAIL);
                indicator.setText(MISSTHROW_STRING);
            }
        });
    }

    @Override
    public String getFXMLFile() {
        return FXML;
    }
}
