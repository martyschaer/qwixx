package controls;

public interface CustomControl {

    /**
     * The file name of the .fxml corresponding to the custom control.
     */
    String getFXMLFile();
}
