import game.QwixxGame;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

    QwixxGame qwixx;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        this.qwixx = new QwixxGame(primaryStage);
        this.qwixx.start();
    }
}
