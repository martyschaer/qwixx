package utils;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDateTime;

/**
 * This is a XML adapter to marshal and unmarshal LocalDateTime values to and from
 * XML files.
 *
 * @author Severin Kaderli
 */
public class LocalDateTimeXmlAdapter extends XmlAdapter<String, LocalDateTime> {
    /**
     * Parse a LocalDateTime from a string.
     *
     * @param value The string to parse
     * @return The LocalDateTime
     */
    public LocalDateTime unmarshal(String value) {
        return LocalDateTime.parse(value);
    }

    /**
     * Turns a LocalDateTime to a string.
     *
     * @param value The LocalDateTime
     * @return The LocalDateTime as a string
     */
    public String marshal(LocalDateTime value) {
        return value.toString();
    }
}
