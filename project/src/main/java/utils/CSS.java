package utils;

import javafx.scene.Node;
import model.Colour;

import java.awt.*;
import java.util.function.Function;

/**
 * This class provides methods to generate FX-CSS.
 */
public class CSS {

    private static int MAX_COLOR_CHANNEL_VALUE = 255;

    public final static Color DARK = new Color(51, 51, 51);
    public final static Color LIGHT = new Color(239, 239, 239);

    static String asHexCode(Color color) {
        return String.format("#%02X%02X%02X", color.getRed(), color.getGreen(), color.getBlue());
    }

    /**
     * Generates a Colour with only 2/3rds the brightness.
     */
    public static class Disabled {

        private static final double INVERTED_BRIGHTNESS_FACTOR = 1.0/3.0;

        public static Color generate(Colour colour){
            return generate(colour.value());
        }

        private static Color generate(Color value) {
            return Transformator.transformDouble(value, n -> n - n * INVERTED_BRIGHTNESS_FACTOR);
        }
    }

    /**
     * Increases the brightness of all components by a factor. Values are clipped at 255.
     */
    public static class Brighten{
        public static Color generate(Colour colour, float brightnessIncrease){
            return generate(colour.value(), brightnessIncrease);
        }

        public static Color generate(Color value, float brightnessIncrease){
            return Transformator.transformDouble(value, n -> {
                double result = n * brightnessIncrease;
                return result > MAX_COLOR_CHANNEL_VALUE ? MAX_COLOR_CHANNEL_VALUE : result;
            });
        }
    }

    /**
     * Try to create a Color that has a good contrast to the given color.
     * <p>
     * It's either DARK or LIGHT, or the inverse if neither fit.
     */
    public static class Contrast {

        public static final int RED_SCALING_FACTOR = 299;
        public static final int GREEN_SCALING_FACTOR = 587;
        public static final int BLUE_SCALING_FACTOR = 114;
        public static final int NORMALIZATION_FACTOR = 1000;
        public static final int CONTRAST_THRESHOLD = 125;

        //TODO tune. doesn't work with Crossed out Purple fields.
        public static Color generate(Colour colour) {
            return generate(colour.value());
        }

        public static Color generate(Color color) {
            if (goodContrastWith(color, LIGHT)) {
                return LIGHT;
            } else if (goodContrastWith(color, DARK)) {
                return DARK;
            } else {
                return invert(color);
            }
        }

        /**
         * When 2 colors have a brightness difference of >=125,
         * the contrast is good enough for our purposes.
         */
        private static boolean goodContrastWith(Color a, Color b) {
            return Math.abs(calculateBrightness(a) - calculateBrightness(b)) >= CONTRAST_THRESHOLD;
        }

        /**
         * Calculates a colors normalized brightness according to:
         * https://www.w3.org/TR/AERT/#color-contrast
         */
        static int calculateBrightness(Color color) {
            return (color.getRed() * RED_SCALING_FACTOR + //
                    color.getGreen() * GREEN_SCALING_FACTOR + //
                    color.getBlue() * BLUE_SCALING_FACTOR) //
                    / NORMALIZATION_FACTOR; //Normalize
        }

        private static Color invert(Color color) {
            return Transformator.transformInt(color, n -> MAX_COLOR_CHANNEL_VALUE - n);
        }
    }

    /**
     * Generates the FX-CSS String for background color.
     */
    public static class Background {
        public static String generate(Colour colour) {
            return generate(colour.value());
        }

        public static String generate(Color color) {
            return String.format("-fx-background-color: %s;", asHexCode(color));
        }
    }

    /**
     * Generates the FX-CSS String for foreground color.
     */
    public static class Foreground {
        public static String generate(Colour colour) {
            return generate(colour.value());
        }

        public static String generate(Color color) {
            return String.format("-fx-text-fill: %s;", asHexCode(color));
        }
    }

    /**
     * Generates the FX-CSS String for border color.
     */
    public static class Border {
        public static String generate(Colour colour) {
            return generate(colour.value());
        }

        public static String generate(Color color) {
            return String.format(" -fx-border-color: %s; -fx-background-insets: 0 0 1 0; -fx-background-radius: 0;", asHexCode(color));
        }
    }

    public static class Opacity{

        public static final double ONE_HUNDRED_PERCENT = 1.0;

        public static String full(){
            return generate(ONE_HUNDRED_PERCENT);
        }

        public static String generate(double opacity){
            return String.format("-fx-opacity: %.1f;", opacity);
        }
    }

    public static class Font {
        /**
         * Generates FX-CSS String for font size.
         */
        public static class Size {
            public static String generate(int size){
                return String.format("-fx-font-size: %d;", size);
            }
        }

        /**
         * Generates FX-CSS String for font weight.
         */
        public static class Weight {
            private static final String BOLD = "bold";
            private static final String LIGHTER = "lighter";
            private static final String NORMAL = "normal";

            public static String bold(){
                return weight(BOLD);
            }

            public static String normal(){
                return weight(NORMAL);
            }

            public static String light(){
                return weight(LIGHTER);
            }

            public static String weight(String weight){
                return String.format("-fx-font-weight: %s;", weight);
            }
        }
    }

    /**
     * Applies the given CSS Strings to the given node
     */
    public static class Applicator {

        /**
         * Sets the given style (replacing the current style)
         */
        public static void set(Node node, String... styles) {
            node.setStyle(concatStyles(styles));
        }

        /**
         * Adds the given style to the current style
         */
        public static void apply(Node node, String... styles) {
            node.setStyle(node.getStyle() + concatStyles(styles));
        }

        /**
         * Removes all style information
         */
        public static void reset(Node node) {
            node.setStyle("");
        }

        /**
         * Just some String concatenation
         */
        private static String concatStyles(String... styles) {
            StringBuilder sb = new StringBuilder();
            for (String style : styles) {
                sb.append(style);
            }
            return sb.toString();
        }
    }

    /**
     * Applies a function to each component of the given color.
     */
    public static class Transformator{
        public static Color transformInt(Color c, Function<Integer, Integer> f){
            return transformDouble(c, i -> f.apply(i).doubleValue());
        }

        public static Color transformDouble(Color c, Function<Integer, Double> f){
            return new Color(f.apply(c.getRed()).intValue(), f.apply(c.getGreen()).intValue(), f.apply(c.getBlue()).intValue());
        }
    }

}
