package utils;

import controls.CustomControl;
import javafx.fxml.FXMLLoader;

import java.io.IOException;

public class CustomControlHelper {
    /**
     * Loads the fxml defined in the CustomControl
     */
    public static void load(CustomControl instance) {
        FXMLLoader fxmlLoader = new FXMLLoader(instance.getClass().getResource(instance.getFXMLFile()));
        fxmlLoader.setRoot(instance);
        fxmlLoader.setController(instance);
        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }
}
