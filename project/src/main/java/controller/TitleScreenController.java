package controller;

import game.QwixxGame;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * This controller represents the title screen of the game.
 *
 * @author Severin Kaderli
 */
public class TitleScreenController extends Controller {
    /**
     * The button to open the about dialog.
     */
    @FXML
    Button aboutButton;

    /**
     * The button to open the statistics window.
     */
    @FXML
    Button statisticsButton;

    /**
     * The button to start the game.
     */
    @FXML
    Button startGameButton;

    /**
     * Create a new instance of the game window.
     *
     * @param game  The game this controller belongs to
     * @param stage The stage for the controller
     */
    public TitleScreenController(QwixxGame game, Stage stage) {
        super(game, "title_screen.fxml", "Qwixx", stage);
        this.stage.setResizable(false);

        this.aboutButton.setOnAction(event -> this.showAboutDialog());

        this.statisticsButton.setOnAction(event -> {
            StatisticsController statisticsWindow = new StatisticsController(this.game, this.stage, false);
            statisticsWindow.show();
        });

        this.startGameButton.setOnAction(event -> {
            PlayerOverviewController playerOverview = new PlayerOverviewController(this.game, this.stage);
            playerOverview.show();
        });

        this.stage.setOnCloseRequest(event -> Platform.exit());
    }

    /**
     * Display the about window
     */
    public void showAboutDialog() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("About");
        alert.setHeaderText(null);
        StringBuilder sb = new StringBuilder();
        sb.append("This is a Java implementation of the German dice game Qwixx.")
                .append("\n©2012 Nürnberger-Spielkarten-Verlag")
                .append("\n\nAuthors:")
                .append("\nSeverin Kaderli <severin.kaderli@gmail.com>")
                .append("\nMarius Schär <contact@mariusschaer.ch>")
                .append("\n\nSource Code can be found on GitHub")
                .append("\nhttps://github.com/martyschaer/qwixx");

        alert.setContentText(sb.toString());
        alert.showAndWait();
    }
}
