package controller;

import game.QwixxGame;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.player.HumanPlayer;
import model.player.Player;
import model.player.PlayerType;

import model.player.robots.CpuGreedyPlayer;
import model.player.robots.CpuRandomPlayer;

import java.util.Objects;


/**
 * The dialog for adding new players to the game.
 *
 * @author Severin Kaderli
 */
public class AddPlayerController extends Controller {
    /**
     * The text field that holds the name for the new player.
     */
    @FXML
    private TextField playerName;

    /**
     * The choice box for selecting the type of the new player.
     */
    @FXML
    private ChoiceBox<PlayerType> playerType;

    /**
     * The button to add the new player.
     */
    @FXML
    private Button addPlayerButton;

    /**
     * The button to cancel adding a player.
     */
    @FXML
    private Button cancelButton;

    /**
     * Creates the add player dialog.
     *
     * @param game The game this controller belongs to
     */
    public AddPlayerController(QwixxGame game) {
        super(game, "add_player_view.fxml", "Add player", new Stage());
        this.stage.initModality(Modality.APPLICATION_MODAL);
        this.playerType.setItems(FXCollections.observableArrayList(PlayerType.values()));
        this.playerType.setValue(PlayerType.values()[0]);
        this.addPlayerButton.setOnAction(event -> this.addPlayer());
        this.cancelButton.setOnAction(event -> this.stage.close());
    }

    /**
     * Adds a new player to the player list and closes the dialog.
     */
    private void addPlayer() {
        if (this.playerName.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("No player name supplied");
            alert.setHeaderText(null);
            alert.setContentText("Please enter a name for the player.");
            alert.showAndWait();
            return;
        }

        Player player;

        switch (this.playerType.getValue()) {
            case CPU_GREEDY_PLAYER:
                player = new CpuGreedyPlayer(this.playerName.getText());
                break;
            case CPU_RANDOM_PLAYER:
                player = new CpuRandomPlayer(this.playerName.getText());
                break;
            case HUMAN_PLAYER:
            default:
                player = new HumanPlayer(this.playerName.getText());
                break;
        }


        this.game.getPlayers().add(player);
        this.stage.close();
    }
}
