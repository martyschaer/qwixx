package controller;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import game.QwixxGame;
import model.game.GameException;

/**
 * This class represents a controller that displays a window based on a
 * fxml template.
 *
 * @author Severin Kaderli
 */
public abstract class Controller {
    /**
     * The game instance this window belongs to.
     */
    protected QwixxGame game;

    /**
     * The title of the window
     */
    protected String windowTitle;

    /**
     * The name of the fxml resource file.
     */
    protected String resourceName;

    /**
     * The stage of the controller
     */
    protected Stage stage;

    /**
     * Create a new window of the given FXML resource and with the given
     * title.
     *
     * @param resourceName The name of the FXML resource
     * @param windowTitle  The title of the window
     * @param stage The stage to use for the controller
     */
    public Controller(QwixxGame game, String resourceName, String windowTitle, Stage stage) {
        this.resourceName = resourceName;
        this.windowTitle = windowTitle;
        this.stage = stage;
        this.game = game;

        try {
            FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource(this.resourceName));
            fxmlLoader.setController(this);
            this.stage.setScene(new Scene(fxmlLoader.load()));
            this.stage.setTitle(this.windowTitle);

        } catch (Exception e) {
            throw new GameException(e);
        }
    }

    /**
     * Return the stage of the controller.
     *
     * @return The stage of the controller
     */
    public Stage getStage() {
        return this.stage;
    }

    /**
     * Shows the window
     */
    public void show() {
        this.stage.show();
    }
}
