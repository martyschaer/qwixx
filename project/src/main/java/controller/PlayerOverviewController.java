package controller;

import controls.DiceControl;
import controls.sheet.SheetControl;
import game.QwixxGame;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import model.player.Player;
import model.sheet.Sheet;

/**
 * The overview of all the players of the game.
 *
 * @author Severin Kaderli
 */
public class PlayerOverviewController extends Controller {
    /**
     * The list of the players.
     */
    ObservableList<Player> players;

    /**
     * The table view that holds all the players.
     */
    @FXML
    TableView<Player> playerTableView;

    /**
     * The table column for the player name.
     */
    @FXML
    TableColumn<Player, String> playerNameColumn;

    /**
     * The table column for the player type.
     */
    @FXML
    TableColumn<Player, String> playerTypeColumn;

    /**
     * The button to open the add player dialog.
     */
    @FXML
    Button addPlayerButton;

    /**
     * The button to continue with the game.
     */
    @FXML
    Button continueButton;

    /**
     * Creates the overview of players.
     *
     * @param game The game this controller belongs to
     */
    public PlayerOverviewController(QwixxGame game, Stage parentStage) {
        super(game, "player_overview.fxml", "Player Overview", parentStage);

        // Prepare the table view
        this.playerTableView.setItems(QwixxGame.getPlayers());
        this.playerNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        this.playerTypeColumn.setCellValueFactory(new PropertyValueFactory<>("type"));
        this.players = this.playerTableView.getItems();

        // Open the add player dialog
        this.addPlayerButton.setOnAction(event -> {
            AddPlayerController dialog = new AddPlayerController(this.game);
            dialog.show();
        });

        this.continueButton.setOnAction(event -> {
            if (this.players.size() < 1) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Not enough players");
                alert.setHeaderText(null);
                alert.setContentText("There needs to be at least one player to start a game.");
                alert.showAndWait();
                return;
            }

            // Open stages for the player sheets
            for (int i = 0; i < this.players.size(); i++) {
                Player player = this.players.get(i);
                Stage stage = new Stage();
                BorderPane root = new BorderPane();
                Sheet sheet = new Sheet(player);
                player.setSheet(sheet);
                stage.setTitle(player.getName());
                stage.setOnCloseRequest(Event::consume);
                Scene scene = new Scene(root);
                scene.getStylesheets().add("style.css");
                stage.setScene(scene);
                stage.sizeToScene();
                stage.setResizable(false);
                stage.setX(20);

                stage.setY((i * 200) + 20);
                root.setCenter(new SheetControl(sheet));
                stage.show();
                this.game.addPlayerStage(stage);
            }

            BorderPane root = new BorderPane();
            root.setCenter(new DiceControl());
            this.stage.setOnCloseRequest(Event::consume);
            Scene scene = new Scene(root);
            this.stage.setScene(scene);
            this.stage.sizeToScene();
            this.stage.setResizable(false);
            this.stage.setX(20);
            this.stage.setY((this.players.size() * 200) + 20);
            this.stage.show();

            QwixxGame.initGame();
            QwixxGame.nextPlayer();
        });
    }
}
