package controller;

import game.QwixxGame;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import model.player.Player;


/**
 * Controller for the game over screen.
 *
 * @author Severin Kaderli
 */
public class GameOverController extends Controller {
    /**
     * The label for the winner information.
     */
    @FXML
    Label winnerLabel;

    /**
     * The button to open the statistics.
     */
    @FXML
    Button statisticsButton;

    /**
     * The button to exit the game.
     */
    @FXML
    Button exitButton;

    /**
     * Create a new game over window.
     *
     * @param game        The game
     * @param parentStage The parent stage
     */
    public GameOverController(QwixxGame game, Stage parentStage) {
        super(game, "game_over_view.fxml", "Thank you for playing!", parentStage);

        this.stage.setOnCloseRequest(event -> Platform.exit());
        this.stage.centerOnScreen();

        Player winner = this.game.getWinner();
        this.winnerLabel.setText("The winner is " + winner.getName() + " with " + winner.getScore() + " points.");

        this.statisticsButton.setOnAction(event -> {
            StatisticsController statisticsWindow = new StatisticsController(this.game, this.stage, true);
            statisticsWindow.show();
        });

        this.exitButton.setOnAction(event -> Platform.exit());
    }
}
