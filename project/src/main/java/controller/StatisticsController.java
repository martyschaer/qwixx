package controller;

import game.QwixxGame;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.game.HighscoreEntry;

/**
 * Window for the game statistics.
 *
 * @author Severin Kaderli
 */
public class StatisticsController extends Controller {
    /**
     * The table view that holds all the players.
     */
    @FXML
    TableView<HighscoreEntry> statisticsTableView;

    /**
     * The table column for the player name of the highscore
     */
    @FXML
    TableColumn<HighscoreEntry, String> playerNameColumn;

    /**
     * The table column for the score of the highscore.
     */
    @FXML
    TableColumn<HighscoreEntry, String> playerScoreColumn;

    /**
     * The table column for the datetime of the highscore.
     */
    @FXML
    TableColumn<HighscoreEntry, String> dateTimeColumn;

    /**
     * The button to open the add player dialog.
     */
    @FXML
    Button backButton;

    /**
     * The x axis of the line chart.
     */
    @FXML
    NumberAxis xAxis;

    /**
     * The y axis of the line chart.
     */
    @FXML
    NumberAxis yAxis;

    /**
     * The line chart for the statistics.
     */
    @FXML
    LineChart<Number, Number> lineChart;

    /**
     * Creates the overview of players.
     */
    public StatisticsController(QwixxGame game, Stage parentStage, boolean isEndOfGame) {
        super(game, "statistics_view.fxml", "Statistics", parentStage);
        this.stage.setResizable(false);

        // Prepare the table view
        this.statisticsTableView.setItems(this.game.getStatistics().getHighscoreList());
        this.playerNameColumn.setCellValueFactory(new PropertyValueFactory<>("playerName"));
        this.playerScoreColumn.setCellValueFactory(new PropertyValueFactory<>("score"));
        this.playerScoreColumn.setSortType(TableColumn.SortType.DESCENDING);
        this.dateTimeColumn.setCellValueFactory(new PropertyValueFactory<>("dateTime"));
        this.statisticsTableView.getSortOrder().clear();
        this.statisticsTableView.getSortOrder().add(this.playerScoreColumn);

        this.setUpAxes();
        this.populateLineChart();

        // Return to the previous window on click on the back button
        this.backButton.setOnAction(event -> {
            if (isEndOfGame) {
                GameOverController gameOverWindow = new GameOverController(game, this.stage);
                gameOverWindow.show();
            } else {
                TitleScreenController titleScreen = new TitleScreenController(this.game, this.stage);
                titleScreen.show();
            }
        });
    }

    /**
     * Set ups the axes of the line chart.
     */
    private void setUpAxes() {
        this.xAxis.setLabel("Rank");
        this.xAxis.setLowerBound(1);
        this.xAxis.setUpperBound(this.game.getStatistics().getHighscoreList().size());
        this.xAxis.setAutoRanging(false);
        this.xAxis.setTickUnit(1);

        this.yAxis.setLabel("Score");
        this.yAxis.setLowerBound(this.game.getStatistics().getLowscore() - 10);
        this.yAxis.setUpperBound(this.game.getStatistics().getHighscore() + 10);
        this.yAxis.setAutoRanging(false);
        this.yAxis.setTickUnit(20);
    }

    /**
     * Populates the line chart with the highscore data.
     */
    private void populateLineChart() {
        LineChart.Series series = new LineChart.Series<Number, Number>();

        for (int i = 0; i < this.game.getStatistics().getHighscoreList().size(); i++) {
            HighscoreEntry entry = this.game.getStatistics().getHighscoreList().get(i);
            LineChart.Data data = new LineChart.Data<Number, Number>(i + 1, entry.getScore());
            series.getData().add(data);
        }

        // Add the data series to the chart
        lineChart.getData().add(series);
    }
}
