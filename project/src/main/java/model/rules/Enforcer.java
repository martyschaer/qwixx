package model.rules;

import model.Colour;
import model.Dice;
import model.player.Player;
import model.player.Status;
import model.sheet.Row;

import java.util.HashMap;

import static model.Colour.WHITE_A;
import static model.Colour.WHITE_B;

/**
 * Used to enforce the rules. Omnipotent. Omniscient. Watches {@link Player}s sleep.
 */
public class Enforcer {

    //keeps track of how many Fields a Player has crossed
    private static HashMap<Player, Integer> crossCounts = new HashMap<>();
    private static final int CROSSINGS_ALLOWED_ACTIVE_PLAYER = 2;
    private static final int CROSSINGS_ALLOWED_PASSIVE_PLAYERS = 1;

    /**
     * Resets the memory about which {@link Player} crossed how many {@link model.sheet.Field}s
     */
    public static void reset(){
        crossCounts.clear();
    }

    /**
     * Attempts to cross a {@link model.sheet.Field}. Returns whether or not that was successful.
     */
    public static boolean cross(Player player, Colour rowColour, int value){
        return cross(player, player.getSheet().getRow(rowColour), value);
    }

    /**
     * Attempts to cross a {@link model.sheet.Field}. Returns whether or not that was successful.
     */
    public static boolean cross(Player player, Row row, int value){
        if(crossable(player, row, value) && hasCrossedAllowedAmountOfFields(player)){
            boolean crossed = player.getSheet().cross(row, value);
            if(crossed){
                //adds a record of the crossing to the tracker
                if(!crossCounts.containsKey(player)){
                    crossCounts.put(player, 1);
                }else{
                    crossCounts.put(player, crossCounts.get(player) + 1);
                }
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if a player has exceeded their allowance of Crosses this turn.
     */
    private static boolean hasCrossedAllowedAmountOfFields(Player player){
        if(crossCounts.containsKey(player)){
            if(activePlayer(player)){
                return crossCounts.get(player) < CROSSINGS_ALLOWED_ACTIVE_PLAYER;
            }else{
                return crossCounts.get(player) < CROSSINGS_ALLOWED_PASSIVE_PLAYERS;
            }
        }
        return true;
    }

    private static boolean crossable(Player player, Row row, int value){
        //if the given row is invalid the Cross cannot be done.
        if(!isValidRow(row)){
            return false;
        }

        if(activePlayer(player)){
            // if the active player is trying to make a move, allow a white OR a row value
            return isValidWhiteValue(value) || isValidRowValue(row, value);
        }else{
            // if another player is trying to make a move, allow only a white value
            return isValidWhiteValue(value);
        }
    }

    /**
     * Returns whether the {@link Player}'s {@link Status} is PLAYING
     */
    private static boolean activePlayer(Player player){
        return Status.PLAYING.equals(player.getPlayStatus());
    }

    /**
     * A valid {@link Row} is simply an active {@link Row}
     */
    private static boolean isValidRow(Row row){
        return !Dice.isDisabled(row.getColour());
    }

    /**
     *  A valid value here, is the sum of the two White {@link Dice}
     */
    private static boolean isValidWhiteValue(int value){
        int white_sum = Dice.getValue(WHITE_A) + Dice.getValue(WHITE_B);

        return value == white_sum;
    }

    /**
     * A valid value here, is the value rolled by the {@link model.Die} of the {@link Colour} of the given {@link Row}
     * multiplied with either of the White {@link model.Die}s
     */
    private static boolean isValidRowValue(Row row, int value){
        int row_white_a_sum = Dice.getValue(row.getColour()) + Dice.getValue(WHITE_A);
        int row_white_b_sum = Dice.getValue(row.getColour()) + Dice.getValue(WHITE_B);

        return value == row_white_a_sum || value == row_white_b_sum;
    }
}
