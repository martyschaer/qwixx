package model;

import javafx.beans.property.BooleanProperty;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Class to hold all instances of {@link Die} and get data from them
 */
public class Dice {
    private static Map<Die, Integer> dice;

    static{
        dice = Stream.of(Colour.values()).map(Die::new).collect(Collectors.toMap(Function.identity(), Die::roll));
        roll();
    }

    /**
     * Rolls every {@link Die} and returns a map of them + the value they each rolled
     */
    public static Map<Die, Integer> roll(){
         dice = dice.keySet().stream().collect(Collectors.toMap(Function.identity(), Die::roll));
         return dice;
    }

    /**
     *  Returns the values of the last roll
     */
    public static Map<Die, Integer> getDice(){
        return dice;
    }

    /**
     * Returns the last rolled value of the {@link Die} of the the given {@link Colour}.
     */
    public static Integer getValue(Colour colour){
        return getValue(new Die(colour));
    }

    /**
     * Returns the last rolled value of the given {@link Die}
     */
    public static Integer getValue(Die die){
        return dice.get(die);
    }

    /**
     * Disables the {@link Die} of the given {@link Colour}
     */
    public static void disable(Colour colour){
        disable(new Die(colour));
    }

    /**
     * Disables the given {@link Die}
     */
    public static void disable(Die die){
        getDie(die).disable();
    }

    /**
     *  Returns whether the {@link Die} of the given {@link Colour} is disabled;
     */
    public static boolean isDisabled(Colour colour) {
        return isDisabled(new Die(colour));
    }

    /**
     *  Returns whether the given {@link Die} is disabled;
     */
    public static boolean isDisabled(Die die) {
        return !getDie(die).enabledProperty().get();
    }

    public static BooleanProperty enabledProperty(Colour colour){
        return enabledProperty(new Die(colour));
    }

    public static BooleanProperty enabledProperty(Die die){
        return getDie(die).enabledProperty();
    }

    /**
     *  Returns the correct instance equal to the given {@link Die}
     */
    private static Die getDie(Die die){
        return dice.keySet().stream() //
                .filter(key -> key.equals(die)) //
                .findFirst().get();
    }
}
