package model.sheet;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import model.Colour;
import model.Dice;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Row {

    private final Colour colour;
    private final RowType type;
    private List<NumberField> fields = new ArrayList<>();
    private final BooleanProperty finished = new SimpleBooleanProperty(false);
    private final FinishedField doneField;
    final Sheet parent;

    public Row(Colour colour, RowType type, Sheet parent){
        this.colour = colour;
        this.type = type;
        this.parent = parent;
        fields.addAll(this.type.getNumbers().stream().map(n -> new NumberField(n, this)).collect(Collectors.toList()));
        doneField = new FinishedField(this);

        // Disable the Die of the same Colour when finished
        this.finished.addListener((observableValue, oldUnused, newBoolean) -> {
            if(newBoolean){
                Dice.disable(colour);
                // Disable the remaining fields to prevent checking
                disableFieldsToTheLeft(this.fields.size() - 1);
            }
        });

        // Listens whether the Dice of the Colour of this Row is disabled. If it is, finish this row.
        Dice.enabledProperty(this.colour).addListener((observableValue, oldUnused, newBoolean) -> this.finished.set(!newBoolean));
    }

    /**
     * When either 6 fields are crossed or the corresponding {@link model.Die} is disabled
     */
    void calculateFinished() {
        this.finished.set(calculateCrossed() >= 6 || Dice.isDisabled(colour));
    }

    int calculateCrossed(){
        return (int) this.fields.stream()
                .filter(NumberField::isCrossed)
                .count();
    }

    void disableFieldsToTheLeft(NumberField field){
        int index = this.fields.indexOf(field);
        disableFieldsToTheLeft(index);
    }

    private void disableFieldsToTheLeft(int index){
        this.fields.subList(0, index) //all Fields to the left of the given one
                .stream() //
                .filter(NumberField::isCrossable) // not the ones already crossed out
                .forEach(NumberField::disable); // disable the remaining fields
    }

    public List<NumberField> getFields() {
        return fields;
    }

    public Colour getColour() {
        return colour;
    }

    public BooleanProperty finished(){
        return this.finished;
    }

    public Sheet getSheet(){
        return this.parent;
    }
}
