package model.sheet;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import model.Colour;
import model.player.Player;
import model.player.Status;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static model.Colour.*;
import static model.sheet.RowType.FORWARD;
import static model.sheet.RowType.REVERSE;

public class Sheet {

    private static final int MAX_NAME_DISPLAY_LENGTH = 10;

    private final Player player;

    private final List<Row> rows = new ArrayList<>();

    public Sheet(Player player){
        this.player = player;
        this.rows.add(new Row(RED, FORWARD, this));
        this.rows.add(new Row(YELLOW, FORWARD, this));
        this.rows.add(new Row(GREEN, REVERSE, this));
        this.rows.add(new Row(BLUE, REVERSE, this));
    }

    public IntegerProperty scoreProperty(){
        return this.player.scoreProperty();
    }

    public IntegerProperty missthrowProperty(){
        return this.player.missthrows();
    }

    public ObjectProperty<Status> playStatus(){
        return this.player.playStatusProperty();
    }

    public String getPlayerName(){
        return this.player.getName();
    }

    /**
     * Provides a playername shortened to 10 characters with ellipsis
     * Returns getPlayerName() otherwise
     */
    public String getPlayerDisplayName(){
        if(getPlayerName().length() > MAX_NAME_DISPLAY_LENGTH){
            return getPlayerName().substring(0, MAX_NAME_DISPLAY_LENGTH) + "...";
        }
        return getPlayerName();
    }

    public List<Row> getRows(){
        return this.rows;
    }

    public int calculateScore() {
        int score = this.rows.stream().mapToInt(row -> {
            long count = row.getFields().stream().filter(NumberField::isCrossed).count();
            return IntStream.rangeClosed(1, (int)count).sum();
        }).sum();

        int missThrows = this.player.getMissthrows() * -5;
        score += missThrows;
        return score;
    }

    /**
     *  Attempts to cross out the {@link Field} with the given value on the {@link Row} of the given {@link Colour}
     *  @return whether crossing was successful or not
     */
    public boolean cross(Colour rowColour, int fieldValue){
        Row row = this.rows.stream().filter(r -> r.getColour().equals(rowColour)).findFirst().get();
        return cross(row, fieldValue);
    }

    /**
     *  Attempts to cross out the {@link Field} with the given value on the given {@link Row}
     *  @return whether crossing was successful or not
     */
    public boolean cross(Row row, int fieldValue){
        return row.getFields().stream().filter(f -> f.getValue() == fieldValue).findFirst().get().cross();
    }

    public Player getPlayer() {
        return this.player;
    }

    public Row getRow(Colour rowColour) {
        return this.rows.stream().filter(row -> rowColour.equals(row.getColour())).findFirst().get();
    }
}
