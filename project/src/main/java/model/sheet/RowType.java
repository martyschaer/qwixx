package model.sheet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public enum RowType {
    FORWARD(IntStream.rangeClosed(2, 12).boxed().collect(Collectors.toList())),
    REVERSE(createReversedList());

    private static List<Integer> createReversedList(){
        List<Integer> reverse = new ArrayList<>(FORWARD.numbers);
        Collections.reverse(reverse);
        return reverse;
    }

    private List<Integer> numbers;

    RowType(List<Integer> numbers) {
        this.numbers = numbers;
    }

    public List<Integer> getNumbers() {
        return this.numbers;
    }
}
