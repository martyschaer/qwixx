package model.sheet;

/**
 * Field at the end of each row, indicating whether this row is done or not.
 */
public class FinishedField extends Field{

    public FinishedField(Row row) {
        super(row);
        this.crossed.bind(row.finished());
    }
}
