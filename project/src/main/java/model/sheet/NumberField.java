package model.sheet;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

/**
 * Represents one of the numbers in a {@link Row}
 */
public class NumberField extends Field{
    private final int value;
    private BooleanProperty enabled;

    public NumberField(int value, Row row){
        super(row);
        this.value = value;
        this.enabled = new SimpleBooleanProperty(true);

        this.crossed.addListener((observableValue, oldBoolean, newBoolean) -> onCrossedChanged(newBoolean));

        this.parent.finished().addListener((observableValue, oldBoolean, newBoolean) -> onFinishedChanged(newBoolean));
    }

    private void onCrossedChanged(boolean crossed){
        if(crossed){
            this.parent.calculateFinished();
        }
    }

    private void onFinishedChanged(boolean finished){
        if(finished){
            this.enabled.set(false);
        }
    }

    public boolean isCrossed() {
        return this.crossed.get();
    }

    public BooleanProperty crossed(){
        return this.crossed;
    }

    public BooleanProperty enabled(){
        return this.enabled;
    }

    public boolean isCrossable(){
        //if this field is the last one, check that 5 have been crossed before
        if(this.parent.getFields().indexOf(this) == this.parent.getFields().size() - 1 && this.parent.calculateCrossed() < 5){
            return false;
        }

        return !this.isCrossed() && this.isEnabled();
    }

    public boolean isEnabled(){
        return this.enabled.get();
    }

    public void disable() {
        this.enabled.set(false);
    }

    boolean cross() {
        if(isCrossable()){
            this.crossed.set(true);
            this.parent.disableFieldsToTheLeft(this);
            return true;
        }
        return false;
    }

    public Row getRow() {
        return parent;
    }

    public int getValue() {
        return value;
    }
}
