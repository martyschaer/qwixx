package model.sheet;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

public abstract class Field {

    final BooleanProperty crossed = new SimpleBooleanProperty(false);
    final Row parent;

    public Field(Row row){
        this.parent = row;
    }

    public Row getRow(){
        return this.parent;
    }

    public BooleanProperty crossed(){
        return this.crossed;
    }
}
