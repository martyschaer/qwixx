package model;

import java.awt.*;

public enum Colour {
    RED(new Color(164, 62, 70)),
    YELLOW(new Color(248, 196, 71)),
    GREEN(new Color(75, 139, 86)),
    BLUE(new Color(35, 106, 190)),
    WHITE_A(new Color(218, 218, 218)),
    WHITE_B(new Color(218, 218, 218));

    private Color val;

    Colour(Color color) {
        this.val = color;
    }

    public Color value() {
        return this.val;
    }
}
