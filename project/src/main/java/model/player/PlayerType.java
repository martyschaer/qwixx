package model.player;

/**
 * This enum defines the different types for the players.
 *
 * @author Severin Kaderli
 */
public enum PlayerType {
    HUMAN_PLAYER,
    CPU_RANDOM_PLAYER,
    CPU_GREEDY_PLAYER;

    /**
     * Returns the player type without an underscore.
     *
     * @return The player type without an underscore
     */
    @Override
    public String toString() {
        return this.name().replace("_", " ");
    }
}
