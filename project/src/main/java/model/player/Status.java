package model.player;

/**
 * Indicates the status that a player is in.
 * Can be one of the following 3:
 * PLAYING  -> it's currently this players turn.
 *          -  "DONE" is displayed on the status label
 *          -  the status label is green
 * WAITING  -> it's another players turn, and the player has to wait
 *          -  "WAIT" is displayed on the status label
 *          -  the status label is orange
 */
public enum Status {
    PLAYING("status_playing", "DONE"),
    WAITING("status_waiting", "WAIT");

    private String cssClass;
    private String text;

    Status(String cssClass, String text){
        this.cssClass = cssClass;
        this.text = text;
    }

    public String text(){
        return this.text;
    }

    public String cssClass(){
        return this.cssClass;
    }

    public static Status getOther(Status status){
        return values()[(status.ordinal()+1) % 2];
    }

    public Status getOther(){
        return Status.getOther(this);
    }
}
