package model.player.robots;

import model.Colour;
import model.Dice;
import model.player.PlayerType;
import model.player.Status;
import model.rules.Enforcer;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static model.Colour.*;

/**
 * A Random CPU {@link model.player.Player}. It chooses {@link model.sheet.Row}s and {@link model.sheet.Field}s
 * at Random and crosses out the first one that's valid. It attempts to do this 10'000 times at most, then just accepts
 * the MissThrow.
 */
public class CpuRandomPlayer extends Robot {

    private static final List<Colour> colours = Arrays.asList(RED, YELLOW, GREEN, BLUE);
    private static final List<Colour> whites = Arrays.asList(WHITE_A, WHITE_B);
    private static final int MAX_ATTEMPTS = 10000;

    public CpuRandomPlayer(String name) {
        super(name);
        this.type = PlayerType.CPU_RANDOM_PLAYER;
    }

    @Override
    void playPassive() {
        int whiteSum = CpuHelper.sumWhiteDice();
        boolean crossed;
        int attempts = 0;
        do {
            attempts++;
            //Choose a Random row to cross out the sum of the white dice on
            Collections.shuffle(colours);
            //try to cross it out
            crossed = Enforcer.cross(this, colours.get(0), whiteSum);
            //if crossing out was not successful, try again.
        } while (!crossed && attempts < MAX_ATTEMPTS);
    }

    @Override
    protected void play() {
        //Roll Dice
        Dice.roll();
        int whiteSum = CpuHelper.sumWhiteDice();
        boolean crossed = false;
        int attempts = 0;
        do {
            attempts++;
            //Choose a Random row to cross out the sum of the white dice on
            Collections.shuffle(colours);
            //try to cross it out
            crossed = Enforcer.cross(this, colours.get(0), whiteSum);
            //if crossing out was not successful, try again.
        } while (!crossed && attempts < MAX_ATTEMPTS);

        // Play the letOthersPlay part of all other bots.
        Robot.letOthersPlay();
        Robot.letHumansPlay();

        attempts = 0;
        do {
            attempts++;
            Collections.shuffle(colours);
            Collections.shuffle(whites);
            //Choose a Random Colour to cross out the sum of this Colour Die on
            Colour colour = colours.get(0);
            int colourWhiteSum = CpuHelper.sumDice(colour, whites.get(0));
            //try to cross it out
            crossed = Enforcer.cross(this, colour, colourWhiteSum);
            //if crossing out was not successful, try again.
        } while (!crossed && attempts < MAX_ATTEMPTS);

        this.setPlayStatus(Status.WAITING);
    }
}
