package model.player.robots;

import model.Dice;
import model.player.PlayerType;
import model.player.Status;
import model.rules.Enforcer;
import model.sheet.Row;

import static model.Colour.WHITE_A;
import static model.Colour.WHITE_B;

/**
 * A Greedy CPU player. Always chooses the leftmost possible value.
 */
public class CpuGreedyPlayer extends Robot {

    public CpuGreedyPlayer(String name) {
        super(name);
        this.type = PlayerType.CPU_GREEDY_PLAYER;
    }

    @Override
    void playPassive() {
        playWhiteDice();
    }

    @Override
    protected void play() {
        //Roll Dice
        Dice.roll();
        playWhiteDice();
        Robot.letOthersPlay();
        Robot.letHumansPlay();

        Row leftMostRow = null;
        int leftMostValue = -1;
        int leftMostIndex = Integer.MAX_VALUE;
        for (int i = 0; i < this.getSheet().getRows().size(); i++) {
            Row row = this.getSheet().getRows().get(i);
            int sumA = CpuHelper.sumDice(row.getColour(), WHITE_A);
            int sumB = CpuHelper.sumDice(row.getColour(), WHITE_B);
            int indexA = calculateIndexOfValue(row, sumA);
            int indexB = calculateIndexOfValue(row, sumB);
            if (indexA < indexB) {
                if (indexA < leftMostIndex) {
                    leftMostValue = sumA;
                    leftMostRow = row;
                }
            } else {
                if (indexB < leftMostIndex) {
                    leftMostValue = sumB;
                    leftMostRow = row;
                }
            }
        }
        if (leftMostRow != null) {
            Enforcer.cross(this, leftMostRow, leftMostValue);
        } else {
            System.out.printf("[WARN] : %s didn't play Colored Dice%n", this.getName());
        }

        this.setPlayStatus(Status.WAITING);
    }

    private int calculateIndexOfValue(Row row, int value) {
        for (int i = 0; i < row.getFields().size(); i++) {
            if (row.getFields().get(i).getValue() == value) {
                return i;
            }
        }
        return Integer.MAX_VALUE;
    }

    private void playWhiteDice() {
        int sum = CpuHelper.sumWhiteDice();
        int minRowIndex = Integer.MAX_VALUE;
        Row minRow = null;
        for (Row row : this.getSheet().getRows()) {
            int minIndex = Integer.MAX_VALUE;
            for (int i = 0; i < row.getFields().size(); i++) {
                if (row.getFields().get(i).getValue() == sum) {
                    minIndex = i;
                    break;
                }
            }
            if (minRowIndex > minIndex) {
                minRowIndex = minIndex;
                minRow = row;
            }
        }
        if (minRow != null) {
            Enforcer.cross(this, minRow, sum);
        } else {
            System.out.printf("[WARN] : %s played no white dice%n", this.getName());
        }
    }
}
