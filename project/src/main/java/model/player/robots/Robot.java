package model.player.robots;

import game.QwixxGame;
import javafx.scene.control.Alert;
import javafx.stage.Modality;
import model.player.HumanPlayer;
import model.player.Player;

public abstract class Robot extends Player {
    public Robot(String name) {
        super(name);
    }

    /**
     * Robots should implement their passive action here.
     * Usually this is just crossing off the value of the sum of the white {@link model.Dice}
     */
    abstract void playPassive();

    /**
     * Calls playPassive of all {@link Player}s that extend this class.
     */
    public static void letOthersPlay() {
        QwixxGame.getPlayers().stream().filter(p -> p instanceof Robot).forEach(r -> ((Robot) r).playPassive());
    }

    /**
     * Gives humans a pop-up box, during which they can make their passive moves.
     */
    public static void letHumansPlay(){
        //if any humans are playing
        if(QwixxGame.getPlayers().stream().anyMatch(p -> p instanceof HumanPlayer)){
            //alert them to take action
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.initModality(Modality.NONE);
            alert.setTitle("Please play now.");
            alert.setHeaderText(null);
            alert.setContentText(String.format("All humans may cross out the sum of the two white dice: %d\nThen press OK.", CpuHelper.sumWhiteDice()));
            alert.showAndWait();
        }
    }
}
