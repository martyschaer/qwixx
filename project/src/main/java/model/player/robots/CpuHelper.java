package model.player.robots;

import model.Colour;
import model.Dice;
import model.Die;

import static model.Colour.WHITE_A;
import static model.Colour.WHITE_B;

public class CpuHelper {
    public static int sumWhiteDice() {
        return sumDice(new Die(WHITE_A), new Die(WHITE_B));
    }


    public static int sumDice(Die a, Die b) {
        int valueA = Dice.getValue(a);
        int valueB = Dice.getValue(b);
        return sum(valueA, valueB);
    }

    public static int sumDice(Colour a, Colour b) {
        int valueA = Dice.getValue(a);
        int valueB = Dice.getValue(b);
        return sum(valueA, valueB);
    }

    /**
     * Returns 0 if any of the {@link Die}s returns 0.
     * This means one of the {@link Die}s was disabled.
     */
    private static int sum(int a, int b) {
        if (a < 1 || b < 1) {
            return 0;
        }
        return a + b;
    }
}
