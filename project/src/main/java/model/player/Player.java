package model.player;

import game.QwixxGame;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import model.sheet.Sheet;

public abstract class Player {
    protected PlayerType type;

    private final String name;

    private final ObjectProperty<Status> playStatus = new SimpleObjectProperty<>(Status.WAITING);
    private final IntegerProperty score = new SimpleIntegerProperty(0);
    private final IntegerProperty missthrows = new SimpleIntegerProperty(0);
    private Sheet sheet;
    private int previousScore = Integer.MAX_VALUE;

    public Player(String name) {
        this.name = name;
        this.playStatus.addListener((observableValue, oldUnused, newStatus) -> {
            if(newStatus.equals(Status.PLAYING)){
                this.play();
            }else if(newStatus.equals(Status.WAITING)){
                this.done();
            }
        });
    }

    public void setSheet(Sheet sheet){
        this.sheet = sheet;
    }

    public Sheet getSheet(){
        if(sheet == null){
            throw new IllegalStateException("Must set Sheet");
        }
        return this.sheet;
    }

    public PlayerType getType() {
        return this.type;
    }
    public String getName() {
        return name;
    }

    public int getScore() {
        return score.get();
    }

    public IntegerProperty scoreProperty() {
        return score;
    }

    public void setPlayStatus(Status status) {
        this.playStatus.setValue(status);
    }

    public ObjectProperty<Status> playStatusProperty() {
        return this.playStatus;
    }

    public Status getPlayStatus(){
        return this.playStatus.get();
    }

    public int getMissthrows(){
        return this.missthrows.get();
    }

    public IntegerProperty missthrows(){
        return this.missthrows;
    }

    private void calculateScore(){
        this.scoreProperty().set(this.sheet.calculateScore());
    }

    /**
     * Used to check whether or not any moves were made
     */
    public int getPreviousScore(){
        return this.previousScore;
    }

    /**
     * Used to chekc whether or not any moves were made
     */
    public void setPreviousScore(int previousScore){
        this.previousScore = previousScore;
    }

    /**
     * Called when the Player enters the PLAYING State
     * Children must call QwixxGame.nextPlayer()
     */
    protected abstract void play();

    /**
     * Automatically calculates the Score at the end of each round
     * and advances to the next player
     */
    void done(){
        calculateScore();
        QwixxGame.nextPlayer();
    }
}
