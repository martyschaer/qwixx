package model.player;

import game.QwixxGame;
import javafx.scene.control.Alert;
import javafx.stage.Modality;
import model.player.robots.CpuHelper;
import model.player.robots.Robot;

public class HumanPlayer extends Player {

    public HumanPlayer(String name) {
        super(name);
        this.type = PlayerType.HUMAN_PLAYER;
    }

    @Override
    protected void play() {
        letRobotsPlay();
    }

    private void letRobotsPlay(){
        //if any Robots are playing
        if(QwixxGame.getPlayers().stream().anyMatch(p -> p instanceof Robot)){
            //alert them to take action
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.initModality(Modality.NONE);
            alert.setTitle("Please play now.");
            alert.setHeaderText(null);
            alert.setContentText(String.format("CLick ok if it's okay for Robots to take their passive turns.", CpuHelper.sumWhiteDice()));
            alert.show();
            alert.setOnCloseRequest(dialogEvent -> Robot.letOthersPlay());
        }
    }
}
