package model.game;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Model for the game statistics.
 *
 * @author Severin Kaderli
 */
@XmlRootElement(name = "statistics")
public class Statistics {
    /**
     * The list of the highscore entries
     */
    private ObservableList<HighscoreEntry> highscoreList;

    /**
     * Create new statistics object with an empty highscore list.
     */
    public Statistics() {
        this.highscoreList = FXCollections.observableArrayList();
    }

    /**
     * Return the highscore list.
     *
     * @return The highscore list
     */
    @XmlElementWrapper(name = "highscoreList")
    @XmlElement(name = "highscore")
    public ObservableList<HighscoreEntry> getHighscoreList() {
        return this.highscoreList;
    }

    /**
     * Sets the highscore list.
     *
     * @param highscoreList The new highscore list
     */
    public void setHighscoreList(ObservableList<HighscoreEntry> highscoreList) {
        this.highscoreList = highscoreList;
    }

    /**
     * Get the highscore
     *
     * @return The highest score from the statistics
     */
    public int getHighscore() {
        int highscore = 0;
        for (HighscoreEntry entry : this.getHighscoreList()) {
            if (entry.getScore() > highscore) {
                highscore = entry.getScore();
            }
        }
        return highscore;
    }

    public int getLowscore() {
        int lowscore = 0;
        for (HighscoreEntry entry : this.getHighscoreList()) {
            if(entry.getScore() < lowscore) {
                lowscore = entry.getScore();
            }
        }
        return lowscore;
    }
}
