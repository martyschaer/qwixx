package model.game;

/**
 * This is an exception class which should be used for exception related
 * to the game game.
 *
 * @author Severin Kaderli
 */
public class GameException extends RuntimeException {
    /**
     * Create a new instance of the exception based on another exception.
     *
     * @param e The parent exception
     */
    public GameException(Exception e) {
        super(e);
    }

    /**
     * Create a new instance of the exception based on a message.
     *
     * @param message The message of the exception
     */
    public GameException(String message) {
        super(message);
    }
}
