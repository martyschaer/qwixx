package model.game;

/**
 * This enum represents the different states of the game.
 */
public enum GameState {
    SETUP,
    ONGOING,
    FINISHED
}
