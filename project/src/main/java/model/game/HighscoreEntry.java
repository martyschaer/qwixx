package model.game;

import utils.LocalDateTimeXmlAdapter;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDateTime;

/**
 * This class represents one entry of the highscore statistic.
 *
 * @author Severin Kaderli
 */
public class HighscoreEntry {
    /**
     * The name of the player for highscore entry.
     */
    private String playerName;

    /**
     * The score of the highscore entry.
     */
    private int score;
    /**
     * The date and time the highscore entry was made.
     */
    private LocalDateTime dateTime;

    /**
     * Creates a new highscore entry, which is empty. This is
     * needed for JAXB to work.
     */
    public HighscoreEntry() {

    }

    /**
     * Create a new highscore entry, with the given data.
     *
     * @param playerName The name of the player
     * @param score      The score
     * @param dateTime   The date time
     */
    public HighscoreEntry(String playerName, int score, LocalDateTime dateTime) {
        this.playerName = playerName;
        this.score = score;
        this.dateTime = dateTime;
    }

    /**
     * Returns the player name of the highscore entry.
     *
     * @return The player name
     */
    public String getPlayerName() {
        return this.playerName;
    }

    /**
     * Sets the player name.
     *
     * @param playerName The new player name
     */
    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    /**
     * Returns the score of the highscore entry.
     *
     * @return The score
     */
    public int getScore() {
        return this.score;
    }

    /**
     * Sets the score.
     *
     * @param score The new score
     */
    public void setScore(int score) {
        this.score = score;
    }

    /**
     * Returns the LocalDateTime of the highscore entry.
     *
     * @return The LocalDateTime
     */
    @XmlJavaTypeAdapter(value = LocalDateTimeXmlAdapter.class)
    public LocalDateTime getDateTime() {
        return this.dateTime;
    }

    /**
     * Sets the date time.
     *
     * @param dateTime The new date time
     */
    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }
}
