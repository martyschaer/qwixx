package model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;

import java.util.Objects;
import java.util.Random;

public class Die{

    private final BooleanProperty enabled = new SimpleBooleanProperty(true);

    public static final int MIN = 1;
    public static final int MAX = 6;

    private final Colour colour;
    private final Random random;
    private final IntegerProperty value;

    public Die(Colour colour) {
        this(colour, new Random());
    }

    Die(Colour colour, Random random) {
        this.colour = colour;
        this.random = random;
        this.value = new SimpleIntegerProperty(1);
    }

    public IntegerProperty getValueProperty() {
        return this.value;
    }

    int roll() {
        if(!this.enabled.get()){
            this.value.setValue(0);
            return 0;
        }
        int rnd = random.nextInt();
        rnd = rnd <= MIN ? rnd * -1 : rnd;
        rnd %= MAX;
        this.value.set(++rnd);
        return this.value.get();
    }

    public Colour getColour() {
        return this.colour;
    }

    public BooleanProperty enabledProperty(){
        return this.enabled;
    }

    public boolean isDisabled(){
        return !this.enabled.get();
    }

    public void disable(){
        this.enabled.set(false);
    }

    public boolean equals(Die that){
        return this.colour.equals(that.colour);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Die die = (Die) o;
        return colour == die.colour;
    }

    @Override
    public int hashCode() {
        return Objects.hash(colour);
    }
}
