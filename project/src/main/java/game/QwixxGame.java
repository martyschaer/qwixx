package game;

import controller.GameOverController;
import controller.TitleScreenController;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import model.Dice;
import model.Die;
import model.game.GameState;
import model.game.HighscoreEntry;
import model.game.Statistics;
import model.player.Player;
import model.player.Status;
import model.rules.Enforcer;
import persistence.StatisticsDAO;
import persistence.xml.XMLStatisticsDAO;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * This is the game class that represents the game.
 *
 * @author Severin Kaderli
 */
public class QwixxGame {
    /**
     * The game stage for the game.
     */
    private static Stage primaryStage;

    /**
     * A list of the players for the current game.
     */
    private static ObservableList<Player> players;

    /**
     * The player that won the game.
     */
    private static Player winner;

    /**
     * A reference to the game object.
     */
    private static QwixxGame game;

    /**
     * The currently active player.
     */
    private static ObjectProperty<Player> currentPlayer = new SimpleObjectProperty<>();

    /**
     * The current state of the game.
     */
    private static ObjectProperty<GameState> currentGameState = new SimpleObjectProperty<>(GameState.SETUP);

    /**
     * The statistics of the game
     */
    private static Statistics statistics;

    /**
     * The stages for the player sheets.
     */
    private static List<Stage> playerStages;

    /**
     * Creates a new instance of the game.
     *
     * @param primaryStage The primary stage which the game uses.
     */
    public QwixxGame(Stage primaryStage) {
        QwixxGame.primaryStage = primaryStage;
        game = this;
        this.setCurrentGameState(GameState.SETUP);
        QwixxGame.players = FXCollections.observableArrayList();
        playerStages = new ArrayList();
        this.loadStatistics();
    }

    /**
     * Return the list of players.
     *
     * @return The list of players
     */
    public static ObservableList<Player> getPlayers() {
        return QwixxGame.players;
    }

    /**
     * Shuffles the Player list
     * Sets the Game State to ONGOING
     */
    public static void initGame() {
        Collections.shuffle(players);
        QwixxGame.currentGameState.set(GameState.ONGOING);
    }

    /**
     * Should be called when the Player is done, by the current Player
     * Sets the next Player in the list to PLAYING
     */
    public static void nextPlayer() {
        if (currentPlayer == null || currentPlayer.get() == null) {
            currentPlayer.set(players.get(0));
        }

        Enforcer.reset();

        currentPlayer.get().setPlayStatus(Status.WAITING);

        checkForFailureCondition();
        checkForTwoRowsFinshedCondition();

        if (currentGameState.get() == GameState.FINISHED) {
            endGame();
            return;
        }

        int index = players.indexOf(currentPlayer.get());
        if (++index >= players.size()) {
            index = 0;
        }

        currentPlayer.set(players.get(index));
        currentPlayer.get().setPlayStatus(Status.PLAYING);
    }

    /**
     * Checks if any Player has 4 or more Missthrows
     * If yes, set GameState to FINISHED
     */
    private static void checkForFailureCondition() {
        //add a MissThrow if the active Players score hasn't changed
        if (currentPlayer.get().getPreviousScore() == currentPlayer.get().getScore()) {
            currentPlayer.get().missthrows().set(currentPlayer.get().getMissthrows() + 1);
        }
        currentPlayer.get().setPreviousScore(currentPlayer.get().getScore());

        // Halt the game if any player reaches above 4 missthrows
        if (players.stream().anyMatch(p -> p.getMissthrows() >= 4)) {
            QwixxGame.currentGameState.set(GameState.FINISHED);
        }
    }

    /**
     * If two or more Rows are in the finished condition (Die of Colour disabled)
     * Finish the game
     */
    private static void checkForTwoRowsFinshedCondition() {
        if (Dice.getDice().keySet().stream().filter(Die::isDisabled).count() >= 2) {
            QwixxGame.currentGameState.set(GameState.FINISHED);
        }
    }

    /**
     * Finish the game and show the results.
     */
    private static void endGame() {
        winner = players.stream().max(Comparator.comparingInt(Player::getScore)).get();

        // Add new scores to statistics and save them
        for (Player player : players) {
            HighscoreEntry entry = new HighscoreEntry(player.getName(), player.getScore(), LocalDateTime.now());
            statistics.getHighscoreList().add(entry);
        }
        StatisticsDAO statisticsDAO = new XMLStatisticsDAO(Configuration.STATISTICS_FILE_NAME);
        statisticsDAO.saveStatistics(statistics);

        // Hide all player stages
        for (Stage stage : playerStages) {
            stage.close();
        }

        // Show game over screen
        GameOverController gameOverWindow = new GameOverController(game, primaryStage);
        gameOverWindow.show();
    }

    /**
     * Loads the game statistics from the XML file.
     */
    private void loadStatistics() {
        StatisticsDAO statisticsDAO = new XMLStatisticsDAO(Configuration.STATISTICS_FILE_NAME);
        statistics = statisticsDAO.loadStatistics();
    }

    /**
     * Returns the game statistics.
     *
     * @return The statistics
     */
    public Statistics getStatistics() {
        return statistics;
    }

    /**
     * Returns the winner.
     *
     * @return The winner
     */
    public Player getWinner() {
        return winner;
    }

    /**
     * Adds a new player stage.
     *
     * @param stage The stage of a player
     */
    public void addPlayerStage(Stage stage) {
        playerStages.add(stage);
    }

    /**
     * Sets the current game state.
     *
     * @param newState The new game state
     */
    public void setCurrentGameState(GameState newState) {
        QwixxGame.currentGameState.set(newState);
    }

    public ObjectProperty<GameState> gameState() {
        return QwixxGame.currentGameState;
    }

    /**
     * Starts the game.
     */
    public void start() {
        TitleScreenController titleScreen = new TitleScreenController(this, primaryStage);
        titleScreen.show();
    }
}
