package game;

/**
 * This class is just used to hold different configuration values for the application.
 *
 * @author Severin Kaderli
 */
public class Configuration {
    /**
     * The file name under which the statistics are saved.
     */
    public static String STATISTICS_FILE_NAME = "statistics.xml";
}
