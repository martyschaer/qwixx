package persistence.xml;

import model.game.Statistics;
import persistence.PersistenceException;
import persistence.StatisticsDAO;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * DAO to save and load game statistics from XML files.
 *
 * @author Severin Kaderli
 */
public class XMLStatisticsDAO implements StatisticsDAO {
    /**
     * The file path of the file which is used for saving / loading the
     * game statistics.
     */
    private Path path;

    /**
     * Create a new DAO using a string as file path.
     *
     * @param path The path to the file as string.
     */
    public XMLStatisticsDAO(String path) {
        this(Paths.get(path));
    }

    /**
     * Creates a new DAO using a path.
     *
     * @param path The path to the file
     */
    public XMLStatisticsDAO(Path path) {
        this.path = path;
        this.checkPathExistence();
    }

    /**
     * Checks if the file path exists and if not creates with an empty statistic
     * model.
     */
    private void checkPathExistence() {
        if (!Files.exists(this.path)) {
            try {
                Files.createFile(this.path);
                this.saveStatistics(new Statistics());
            } catch (IOException e) {
                throw new PersistenceException(e);
            }
        }
    }

    /**
     * Saves the given game statistics
     *
     * @param statistics The game statistics
     */
    public void saveStatistics(Statistics statistics) {
        try {
            JAXBContext context = JAXBContext.newInstance(Statistics.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(statistics, Files.newOutputStream(this.path));
        } catch (Exception e) {
            throw new PersistenceException(e);
        }
    }

    /**
     * Load game statistics from the XML file.
     *
     * @return The loaded game statistics
     */
    public Statistics loadStatistics() {
        try {
            JAXBContext context = JAXBContext.newInstance(Statistics.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            return (Statistics) unmarshaller.unmarshal(Files.newInputStream(this.path));
        } catch (Exception e) {
            throw new PersistenceException(e);
        }
    }
}
