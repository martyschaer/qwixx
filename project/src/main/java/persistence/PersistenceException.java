package persistence;

/**
 * This is an exception class which should be used for exception related
 * to data persistence.
 *
 * @author Severin Kaderli
 */
public class PersistenceException extends RuntimeException {
    /**
     * Create a new instance of the exception based on another exception.
     *
     * @param e The parent exception
     */
    public PersistenceException(Exception e) {
        super(e);
    }
}
