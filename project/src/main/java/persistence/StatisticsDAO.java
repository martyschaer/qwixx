package persistence;

import model.game.Statistics;

/**
 * Interface for allowing data access objects of game statistics.
 *
 * @author Severin Kaderli
 */
public interface StatisticsDAO {
    /**
     * Saves the given statistics.
     */
    void saveStatistics(Statistics statistics);

    /**
     * Loads statistics and returns a StatisticsController object.
     *
     * @return The loaded statistics
     */
    Statistics loadStatistics();
}
