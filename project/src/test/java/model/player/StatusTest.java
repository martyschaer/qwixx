package model.player;

import org.junit.Test;

import static org.junit.Assert.*;

public class StatusTest {

    @Test
    public void getOther() {
        Status waiting = Status.WAITING;
        Status playing = Status.PLAYING;
        assertEquals(playing, waiting.getOther());
        assertEquals(waiting, playing.getOther());
    }
}