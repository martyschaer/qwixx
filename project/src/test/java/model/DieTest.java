package model;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.*;

public class DieTest {
    private Die die;
    private int iterations = 100000;
    private List<Integer> rolledValues = new ArrayList<>(iterations);
    private static final double ACCEPTABLE_DEVIATION_FROM_EVEN_DISTRIBUTION = 0.015;//1.5%

    /**
     * Rolls a die 100'000 times before each test.
     */
    @Before
    public void before() {
        this.die = new Die(Colour.WHITE_A, new Random());
        for (int i = 0; i < iterations; i++) {
            rolledValues.add(die.roll());
        }
    }

    /**
     * This test tries to assure within reasonable statistical certainty
     * that the {@code Die} only generates values that lie within MIN and MAX
     * of {@code Die}.
     */
    @Test
    public void shouldGenerateNumbersBetweeenMinAndMax() {
        assertTrue(rolledValues.stream().allMatch(n -> Die.MIN <= n && n <= Die.MAX));
    }

    /**
     * This test looks at the deviation within the values generated
     * and makes sure that they are within a reasonable range of below 1%.
     */
    @Test
    public void shouldGenerateNumbersReasonablyDistributed() {
        Collection<Long> counts = rolledValues.stream() //
                .collect(Collectors.groupingBy(n -> n, Collectors.counting())) // count the occurrences of each number
                .values();
        double avg = LongStream.of(counts.stream() // take the average over all occurrences
                .mapToLong(Long::longValue)
                .toArray())
                .average()
                .orElse(-1);
        double deviation = counts.stream()              //calculate the deviation of each count
                .mapToDouble(n -> Math.abs(n - avg))
                .average()                              //take the average deviation
                .orElse(-1);

        deviation /= avg; //deviation in percent

        assertThat("Deviation larger than acceptable value", //
                deviation,
                lessThan(ACCEPTABLE_DEVIATION_FROM_EVEN_DISTRIBUTION));

    }
}
