package persistence.xml;

import model.game.Statistics;
import org.junit.Test;
import persistence.PersistenceException;

/**
 * @author Severin Kaderli
 */
public class XMLStatisticsDAOTest {
    /**
     * Tests that a PersistenceException is thrown when saving to a non
     * existing file.
     */
    @Test(expected = PersistenceException.class)
    public void shouldThrowPersistenceExceptionWhenSavingNonExistingFile() {
        XMLStatisticsDAO xmlDAO = new XMLStatisticsDAO("This/is/not/a/real/path.xml");
        xmlDAO.saveStatistics(new Statistics());
    }

    /**
     * Tests that a PersistenceException is thrown when loading from a non
     * existing file.
     */
    @Test(expected = PersistenceException.class)
    public void shouldThrowPersistenceExceptionWhenLoadingNonExistingFile() {
        XMLStatisticsDAO xmlDAO = new XMLStatisticsDAO("This/is/not/a/real/path.xml");
        xmlDAO.loadStatistics();
    }
}