package utils;

import model.Colour;
import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.assertEquals;

public class CSSTest {
    private Colour red = Colour.RED;
    private Color awtRed = red.value();
    private String hexRed = "#A43E46";
    private int brightnessRed = 93;

    private Colour yellow = Colour.YELLOW;
    private Color awtYellow = yellow.value();
    private String hexYellow = "#F8C447";
    private int brightnessYellow = 197;

    @Test
    public void shouldConvertToHex() {
        String red = CSS.asHexCode(awtRed);
        String blue = CSS.asHexCode(awtYellow);

        assertEquals(hexRed, red);
        assertEquals(hexYellow, blue);
    }

    @Test
    public void shouldCalculateContrast() {
        Color redContrast = CSS.Contrast.generate(red);
        Color yellowContrast = CSS.Contrast.generate(yellow);

        assertEquals(CSS.LIGHT, redContrast);
        assertEquals(CSS.DARK, yellowContrast);
    }

    @Test
    public void shouldNotGenerateDifferentlyDependingOnInputModel() {
        Color a = CSS.Contrast.generate(red);
        Color b = CSS.Contrast.generate(awtRed);

        assertEquals(a, b);
    }

    @Test
    public void shouldCalculateBrightnessAccurately() {
        int red = CSS.Contrast.calculateBrightness(awtRed);
        int yellow = CSS.Contrast.calculateBrightness(awtYellow);

        assertEquals(brightnessRed, red);
        assertEquals(brightnessYellow, yellow);
    }
}
