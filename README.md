# qwixx
An implementation of the board game Qwixx in JavaFX for *n* human players and *m* computer players.

## Build Status
[![Build Status](https://travis-ci.com/martyschaer/qwixx.svg?token=sbSe1s25snCdas4kvx7G&branch=develop)](https://travis-ci.com/martyschaer/qwixx)

Our build runs all tests, and compiles the project.

# Requirements
- All game rules are properly implemented (see Rules section)
- multiple game modes
  * single human player
  * multiple human players
  * multiple humans and 0 or more computer players
  * one or more computer players (simulation)
- JavaFX UI
- clean code, MVC, tests
- Statistics (XML and Graphical)

## Game Description
Qwixx is a multiplayer dice game. Every player has a piece of paper, pictured below. The goal is to cross out as many numbers as possible on each row, from left to right.
The player does not have to start with the leftmost number, but may not cross out any numbers more left than any number already crossed out (an arbitrary number of numbers can be skipped).

![Game Sheet](images/sheet.jpg)

### Game progress
- Every player gets a game sheet
- A player gets randomly chosen to be the first active player

Then, the following process happens (Step 1):
1) The acitve player throws all 6 dice (Red, Blue, Green, Yellow, 2x White)
2) The active player sums up the pips on the white dice and announces it
3) Every player **may** now cross out the sum in one row of **any colour**.

Afterwards (Step 2):
1) The active player sums a white die with **one die of any colour**.
2) The acitve player crosses out the sum in the row of the chosen colour.

This process is repeated for the next player, ....

#### Failure condition:
If the active player was unable to cross out any number in either of the above steps, they **must** mark a field in "Fehlwürfe".

### Finishing a row
To close a row (Cross out the rightmost number) a player must:
- have at least 5 numbers in that row crossed out.
- cross out the padlock at the end of the row

The die of the corresponding colour is removed from the game.

### The end of the game
The game can end in two ways:
1) a player has had 4 "Fehlwürfe"
2) Two rows are finished

If any of these conditions are met, even during the first step in the game process, the game ends **immediately**.

### Counting
The number of crossed out numbers in each row are counted. There is a map below the four rows telling us how many points result from each number of crosses.

The scores of each row are summed up, then 5 points per "Fehlwurf" are subtracted.

This results in the final score. The player with the highest score wins.
